package ru.t1.nikitushkina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
