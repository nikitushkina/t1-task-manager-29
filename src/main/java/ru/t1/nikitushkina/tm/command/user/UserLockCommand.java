package ru.t1.nikitushkina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user by login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN OF USER:");
        @Nullable final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
